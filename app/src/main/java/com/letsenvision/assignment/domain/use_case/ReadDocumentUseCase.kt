package com.letsenvision.assignment.domain.use_case

import com.letsenvision.assignment.common.helpers.Resource
import com.letsenvision.assignment.domain.model.DocumentModel
import com.letsenvision.assignment.domain.repository.DocumentRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.File
import javax.inject.Inject

class ReadDocumentUseCase @Inject constructor(
    private val documentRepository: DocumentRepository
) {
    operator fun invoke(file: File): Flow<Resource<DocumentModel>> = flow {
        try {
            emit(Resource.loading(data = null))
            val data = documentRepository.readDocument(file)
            emit(Resource.success(data = data))
        } catch (e: HttpException) {
            val message = e.localizedMessage ?: "Unknown error occurred"
            emit(Resource.error(msg = message, data = null))
        } catch (e: Exception) {
            emit(Resource.error(msg = e.localizedMessage ?: "", data = null))
        }
    }
}