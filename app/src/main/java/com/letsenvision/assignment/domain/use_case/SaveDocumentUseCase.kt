package com.letsenvision.assignment.domain.use_case

import com.letsenvision.assignment.common.helpers.Resource
import com.letsenvision.assignment.domain.model.DocumentModel
import com.letsenvision.assignment.domain.repository.DocumentRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import javax.inject.Inject

class SaveDocumentUseCase @Inject constructor(
    private val repository: DocumentRepository
) {
    operator fun invoke(document: DocumentModel): Flow<Resource<DocumentModel>> = flow {
        try {
            emit(Resource.loading(data = null))
            repository.saveDocumentToLibrary(document)
            emit(Resource.success(data = document))
        } catch (e: HttpException) {
            val message = e.localizedMessage ?: "Unknown error occurred"
            emit(Resource.error(msg = message, data = null))
        } catch (e: Exception) {
            emit(Resource.error(msg = e.localizedMessage ?: "", data = null))
        }
    }
}