package com.letsenvision.assignment.domain.model

import android.os.Parcelable
import com.letsenvision.assignment.data.remote.dto.Paragraph
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class DocumentModel(
    val id: Long,
    val paragraphs: List<Paragraph>,
    var date: Date
) : Parcelable