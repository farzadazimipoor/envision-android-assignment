package com.letsenvision.assignment.domain.repository

import com.letsenvision.assignment.data.db.entity.DocumentEntity
import com.letsenvision.assignment.domain.model.DocumentModel
import kotlinx.coroutines.flow.Flow
import java.io.File

interface DocumentRepository {
    suspend fun readDocument(file: File): DocumentModel
    suspend fun saveDocumentToLibrary(document: DocumentModel)
    fun getDocumentsList(): Flow<List<DocumentEntity>>
}