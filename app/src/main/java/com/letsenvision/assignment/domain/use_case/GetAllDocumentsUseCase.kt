package com.letsenvision.assignment.domain.use_case

import com.letsenvision.assignment.data.db.entity.DocumentEntity
import com.letsenvision.assignment.domain.repository.DocumentRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllDocumentsUseCase @Inject constructor(
    private val repository: DocumentRepository
) {
    operator fun invoke(): Flow<List<DocumentEntity>> {
        return repository.getDocumentsList()
    }
}