package com.letsenvision.assignment.common

object Constants {
    const val BASE_URL = "https://letsenvision.app/api/"
    const val CAPTURE_FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
    const val CAPTURE_TAG = "CameraX"
}