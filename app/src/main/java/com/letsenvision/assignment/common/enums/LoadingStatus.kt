package com.letsenvision.assignment.common.enums

enum class LoadingStatus {
    SUCCESS,
    ERROR,
    LOADING,
    UNAUTHORIZED
}