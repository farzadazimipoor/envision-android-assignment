package com.letsenvision.assignment.common.extensions

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

fun File.createMultipartBody(fieldName: String, type: String): MultipartBody.Part {
    return MultipartBody.Part.createFormData(fieldName, this.name, RequestBody.create(MediaType.parse(type), this))
}