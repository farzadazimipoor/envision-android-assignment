package com.letsenvision.assignment.common.helpers

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

object PermissionHelper {

    const val REQUEST_CODE = 2000

    private var units = mutableMapOf<String, (Boolean) -> Unit>()

    fun requestPermission(activity: Activity, permission: String, unit: (isGranted: Boolean) -> Unit) {
        if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED)
            unit.invoke(true)
        else {
            units[permission] = unit
            ActivityCompat.requestPermissions(activity, arrayOf(permission), REQUEST_CODE)
        }
    }

    fun setRequestPermissionsResult(permission: String, isGranted: Boolean) {
        units[permission]?.invoke(isGranted)
    }
}