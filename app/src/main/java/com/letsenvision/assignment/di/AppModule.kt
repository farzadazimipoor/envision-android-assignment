package com.letsenvision.assignment.di

import android.app.Application
import com.letsenvision.assignment.data.db.DocumentDao
import com.letsenvision.assignment.data.db.EnvisionDb
import com.letsenvision.assignment.data.remote.EnvisionApi
import com.letsenvision.assignment.data.repository.DocumentRepositoryImpl
import com.letsenvision.assignment.domain.repository.DocumentRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelBuilderModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideEnvisionApi(): EnvisionApi {
        return EnvisionApi.create()
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): EnvisionDb {
        return EnvisionDb.create(app.applicationContext, false)
    }

    @Singleton
    @Provides
    fun provideDocumentDao(db: EnvisionDb): DocumentDao {
        return db.captures()
    }

    @Singleton
    @Provides
    fun provideDocumentRepository(api: EnvisionApi, dao: DocumentDao): DocumentRepository {
        return DocumentRepositoryImpl(api, dao)
    }
}