package com.letsenvision.assignment.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.letsenvision.assignment.common.annotation.ViewModelKey
import com.letsenvision.assignment.common.helpers.ViewModelFactory
import com.letsenvision.assignment.presentation.ui.main.capture.camera.CaptureCameraViewModel
import com.letsenvision.assignment.presentation.ui.main.capture.result.CaptureResultViewModel
import com.letsenvision.assignment.presentation.ui.main.library.LibraryViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelBuilderModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(CaptureCameraViewModel::class)
    abstract fun bindCaptureCameraViewModel(vm: CaptureCameraViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CaptureResultViewModel::class)
    abstract fun bindCaptureResultViewModel(vm: CaptureResultViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LibraryViewModel::class)
    abstract fun bindLibraryViewModel(vm: LibraryViewModel): ViewModel
}