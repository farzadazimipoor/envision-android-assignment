package com.letsenvision.assignment.di

import com.letsenvision.assignment.presentation.common.BaseFragment
import com.letsenvision.assignment.presentation.ui.main.capture.camera.CaptureCameraFragment
import com.letsenvision.assignment.presentation.ui.main.capture.CaptureFragment
import com.letsenvision.assignment.presentation.ui.main.capture.result.CaptureResultFragment
import com.letsenvision.assignment.presentation.ui.main.library.LibraryFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeBaseFragment(): BaseFragment

    @ContributesAndroidInjector
    abstract fun contributeCaptureFragment(): CaptureFragment

    @ContributesAndroidInjector
    abstract fun contributeCaptureCameraFragment(): CaptureCameraFragment

    @ContributesAndroidInjector
    abstract fun contributeCaptureResultFragment(): CaptureResultFragment

    @ContributesAndroidInjector
    abstract fun contributeLibraryFragment(): LibraryFragment
}