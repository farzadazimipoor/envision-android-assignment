package com.letsenvision.assignment.presentation.ui.main.capture.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.letsenvision.assignment.R
import com.letsenvision.assignment.common.enums.LoadingStatus
import com.letsenvision.assignment.data.remote.dto.Paragraph
import com.letsenvision.assignment.databinding.FragmentCaptureResultBinding
import com.letsenvision.assignment.presentation.common.BaseFragment
import com.letsenvision.assignment.presentation.ui.main.MainActivity
import java.util.*
import javax.inject.Inject

class CaptureResultFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentCaptureResultBinding

    private lateinit var viewModel: CaptureResultViewModel

    private val args: CaptureResultFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_capture_result, container, false, dataBindingComponent)
        binding.buttonDisabled = false
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[CaptureResultViewModel::class.java]

        val document = args.documentResult

        binding.text = prepareText(document.paragraphs)

        handleOnBackPressed()

        initSaveDocumentObserver()

        binding.button.setOnClickListener {
            if (binding.buttonDisabled == false) {
                document.date = Calendar.getInstance().time
                viewModel.saveDocument(document)
            }
        }
    }

    private fun initSaveDocumentObserver() {
        viewModel.saveDocumentResults.observe(this, {
            binding.buttonDisabled = it.status == LoadingStatus.LOADING || it.status == LoadingStatus.SUCCESS
            if (it.status == LoadingStatus.SUCCESS) {
                (requireActivity() as MainActivity).showSuccessSnackBar()
            }
        })
    }

    private fun prepareText(paragraphs: List<Paragraph>): String {
        var result = ""
        paragraphs.forEach {
            result += it.paragraph + " "
        }
        return result
    }

    private fun handleOnBackPressed() {
        val backListener = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                this.remove()
                findNavController().popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(backListener)
    }
}