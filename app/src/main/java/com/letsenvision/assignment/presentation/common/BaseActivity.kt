package com.letsenvision.assignment.presentation.common

import android.content.pm.PackageManager.PERMISSION_GRANTED
import androidx.appcompat.app.AppCompatActivity
import com.letsenvision.assignment.common.helpers.PermissionHelper
import com.letsenvision.assignment.di.Injectable
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

open class BaseActivity : AppCompatActivity(), HasAndroidInjector, Injectable {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionHelper.REQUEST_CODE && grantResults.isNotEmpty() && permissions.isNotEmpty()) {
            PermissionHelper.setRequestPermissionsResult(permissions[0], grantResults[0] == PERMISSION_GRANTED)
        }
    }
}