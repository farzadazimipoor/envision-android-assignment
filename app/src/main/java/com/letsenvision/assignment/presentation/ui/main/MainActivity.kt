package com.letsenvision.assignment.presentation.ui.main

import android.graphics.Color
import android.os.Bundle
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.letsenvision.assignment.R
import com.letsenvision.assignment.databinding.ActivityMainBinding
import com.letsenvision.assignment.presentation.common.BaseActivity

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewPager = binding.viewPager
        setupTabs()
    }

    private fun setupTabs() {
        val mainPagerAdapter = MainPagerAdapter(this, supportFragmentManager)
        viewPager.adapter = mainPagerAdapter
        val tabs: TabLayout = binding.tabs
        tabs.getTabAt(0)?.contentDescription = getString(R.string.content_capture_tab)
        tabs.getTabAt(1)?.contentDescription = getString(R.string.content_navigate_library)
        tabs.setupWithViewPager(viewPager)
    }

    fun showSuccessSnackBar() {
        val snackBar = Snackbar.make(binding.root, getString(R.string.text_saved_to_library), 10000).setAction(getString(R.string.go_to_library)) {
            viewPager.currentItem = 1
        }
        snackBar.setActionTextColor(Color.BLUE)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(Color.WHITE)
        val textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.GRAY)
        textView.textSize = 14f
        snackBar.show()
    }
}