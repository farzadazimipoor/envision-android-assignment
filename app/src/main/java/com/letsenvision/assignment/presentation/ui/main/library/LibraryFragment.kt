package com.letsenvision.assignment.presentation.ui.main.library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.letsenvision.assignment.R
import com.letsenvision.assignment.common.helpers.AppExecutors
import com.letsenvision.assignment.databinding.FragmentLibraryBinding
import com.letsenvision.assignment.presentation.common.BaseFragment
import javax.inject.Inject

class LibraryFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var binding: FragmentLibraryBinding

    private lateinit var viewModel: LibraryViewModel

    private lateinit var adapter: LibraryListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_library, container, false, dataBindingComponent)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[LibraryViewModel::class.java]

        initAdapterAndLibraryObserver()

        initRecyclerView()
    }

    private fun initAdapterAndLibraryObserver() {
        adapter = LibraryListAdapter(appExecutors, dataBindingComponent) { document ->
            DocumentDetailDialog(requireContext(), document).show()
        }

        viewModel.documentsResults.observe(viewLifecycleOwner, {
            if (!it.any()) {
                // binding.notFound.root.visibility = View.VISIBLE
                // binding.recentRecyclerView.visibility = View.GONE
            } else {
                // binding.notFound.root.visibility = View.GONE
                binding.recyclerView.visibility = View.VISIBLE
                adapter.submitList(it)
            }
        })

        viewModel.getAllDocuments()
    }

    private fun initRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
    }

    companion object {
        fun newInstance() = LibraryFragment()
    }
}