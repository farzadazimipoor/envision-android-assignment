package com.letsenvision.assignment.presentation.ui.main.capture

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.letsenvision.assignment.R
import com.letsenvision.assignment.databinding.FragmentCaptureBinding
import com.letsenvision.assignment.presentation.common.BaseFragment

class CaptureFragment : BaseFragment() {

    private lateinit var binding: FragmentCaptureBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_capture,
            container,
            false,
            dataBindingComponent
        )
        return binding.root
    }

    companion object {
        fun newInstance() = CaptureFragment()
    }
}