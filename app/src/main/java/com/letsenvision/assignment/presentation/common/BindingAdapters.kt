package com.letsenvision.assignment.presentation.common

import android.view.View
import androidx.databinding.BindingAdapter

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("isVisible")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("isEnabled")
    fun disableEnable(view: View, isEnabled: Boolean) {
        view.isEnabled = isEnabled
    }
}