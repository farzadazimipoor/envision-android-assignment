package com.letsenvision.assignment.presentation.ui.main.capture.result

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.letsenvision.assignment.common.helpers.Resource
import com.letsenvision.assignment.domain.model.DocumentModel
import com.letsenvision.assignment.domain.use_case.SaveDocumentUseCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class CaptureResultViewModel @Inject constructor(
    private val saveDocumentUseCase: SaveDocumentUseCase
) : ViewModel() {

    private var _saveDocumentResults: MutableLiveData<Resource<DocumentModel>> = MutableLiveData()

    var saveDocumentResults: LiveData<Resource<DocumentModel>> = _saveDocumentResults

    fun saveDocument(document: DocumentModel) = viewModelScope.launch {
        saveDocumentUseCase.invoke(document).collect { values ->
            _saveDocumentResults.value = values
        }
    }
}