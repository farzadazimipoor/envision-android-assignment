package com.letsenvision.assignment.presentation.ui.main.library

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.letsenvision.assignment.data.db.entity.toDocumentModel
import com.letsenvision.assignment.domain.model.DocumentModel
import com.letsenvision.assignment.domain.use_case.GetAllDocumentsUseCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class LibraryViewModel @Inject constructor(
    private val getAllDocumentsUseCase: GetAllDocumentsUseCase
) : ViewModel() {

    private val _documentsResults: MutableLiveData<List<DocumentModel>> = MutableLiveData()

    var documentsResults: LiveData<List<DocumentModel>> = _documentsResults

    fun getAllDocuments() = viewModelScope.launch {
        getAllDocumentsUseCase.invoke().collect { values ->
            _documentsResults.value = values.map { x -> x.toDocumentModel() }
        }
    }
}