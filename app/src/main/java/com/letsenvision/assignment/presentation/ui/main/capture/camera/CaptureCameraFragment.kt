package com.letsenvision.assignment.presentation.ui.main.capture.camera

import android.Manifest
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.letsenvision.assignment.R
import com.letsenvision.assignment.common.Constants
import com.letsenvision.assignment.common.enums.LoadingStatus
import com.letsenvision.assignment.common.helpers.PermissionHelper
import com.letsenvision.assignment.databinding.FragmentCaptureCameraBinding
import com.letsenvision.assignment.domain.model.DocumentModel
import com.letsenvision.assignment.presentation.common.BaseFragment
import com.letsenvision.assignment.presentation.ui.main.capture.OcrBusyDialog
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.inject.Inject

class CaptureCameraFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentCaptureCameraBinding

    private lateinit var viewModel: CaptureCameraViewModel

    private var imageCapture: ImageCapture? = null

    private lateinit var outputDirectory: File
    private lateinit var cameraExecutor: ExecutorService
    private lateinit var busyDialog: OcrBusyDialog

    private var requestPermissionAgain = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_capture_camera, container, false, dataBindingComponent)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[CaptureCameraViewModel::class.java]
        busyDialog = OcrBusyDialog(requireContext())
        initCameraWithPermissions()
        initReadDocumentObserver()
    }

    override fun onResume() {
        super.onResume()
        if (requestPermissionAgain) {
            initCameraWithPermissions()
        }
        requestPermissionAgain = true
    }

    private fun initReadDocumentObserver() {
        viewModel.readDocumentResult.observe(this, {
            binding.resource = it
            if (it.status == LoadingStatus.LOADING) {
                busyDialog.show()
            } else {
                busyDialog.dismiss()
            }
            if (it.status == LoadingStatus.SUCCESS) {
                if (it.data != null && it.data.paragraphs.any()) {
                    showCaptureResult(it.data)
                } else {
                    Toast.makeText(requireContext(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show()
                }
            }
            if (it.status == LoadingStatus.ERROR) {
                Toast.makeText(requireContext(), it.message ?: getString(R.string.unknown_error), Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun initCameraWithPermissions() {
        PermissionHelper.requestPermission(requireActivity(), Manifest.permission.CAMERA) { isGranted ->
            if (isGranted) {
                outputDirectory = getOutputDirectory()
                cameraExecutor = Executors.newSingleThreadExecutor()
                startCamera()
                binding.cameraCaptureButton.setOnClickListener {
                    PermissionHelper.requestPermission(requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) { hasStoragePermission ->
                        if (hasStoragePermission) {
                            takePhoto()
                        } else {
                            Toast.makeText(requireContext(), getString(R.string.storage_permission_not_granted), Toast.LENGTH_LONG).show()
                            stopLoader()
                        }
                    }
                    requestPermissionAgain = false
                }
            } else {
                Toast.makeText(requireContext(), getString(R.string.camera_permission_not_granted), Toast.LENGTH_LONG).show()
                stopLoader()
            }
        }
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener({
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            val preview = Preview.Builder().build().also {
                it.setSurfaceProvider(binding.viewFinder.surfaceProvider)
            }

            imageCapture = ImageCapture.Builder().build()

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                cameraProvider.unbindAll()

                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)

            } catch (exc: Exception) {
                Log.e(Constants.CAPTURE_TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun takePhoto() {
        startLoader()

        val imageCapture = imageCapture ?: return

        val outputOptions = ImageCapture.OutputFileOptions.Builder(outputDirectory).build()

        imageCapture.takePicture(
            outputOptions, ContextCompat.getMainExecutor(requireContext()), object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(Constants.CAPTURE_TAG, "Photo capture failed: ${exc.message}", exc)
                    stopLoader()
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    viewModel.readDocument(outputDirectory)
                }
            })
    }

    private fun getOutputDirectory(): File {
        val nameFormat = SimpleDateFormat(Constants.CAPTURE_FILENAME_FORMAT, Locale.US).format(System.currentTimeMillis()) + ".jpg"
        return File(requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), nameFormat)
    }

    private fun startLoader() {
        viewModel.emitManualLoadingStatus(LoadingStatus.LOADING)
    }

    private fun stopLoader() {
        viewModel.emitManualLoadingStatus(LoadingStatus.ERROR)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this::cameraExecutor.isInitialized) {
            cameraExecutor.shutdown()
        }
    }

    private fun showCaptureResult(result: DocumentModel) {
        findNavController().navigate(
            CaptureCameraFragmentDirections.actionCaptureCameraFragmentToCaptureResultFragment(result)
        )
    }
}