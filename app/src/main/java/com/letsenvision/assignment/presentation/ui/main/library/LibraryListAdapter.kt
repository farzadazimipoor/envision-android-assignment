package com.letsenvision.assignment.presentation.ui.main.library

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.letsenvision.assignment.R
import com.letsenvision.assignment.common.helpers.AppExecutors
import com.letsenvision.assignment.databinding.DocumentItemViewBinding
import com.letsenvision.assignment.domain.model.DocumentModel
import com.letsenvision.assignment.presentation.common.DataBoundListAdapter
import java.text.SimpleDateFormat

class LibraryListAdapter(
    appExecutors: AppExecutors,
    private val dataBindingComponent: DataBindingComponent,
    private val clickCallback: ((DocumentModel) -> Unit)?
) : DataBoundListAdapter<DocumentModel, DocumentItemViewBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<DocumentModel>() {
        override fun areItemsTheSame(oldItem: DocumentModel, newItem: DocumentModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: DocumentModel, newItem: DocumentModel): Boolean {
            return oldItem == newItem
        }
    }
) {
    override fun createBinding(parent: ViewGroup): DocumentItemViewBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.document_item_view, parent, false, dataBindingComponent
        )
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    override fun bind(binding: DocumentItemViewBinding, item: DocumentModel) {
        val formatter = SimpleDateFormat("dd/MM/yy HH:mm")
        binding.txtDate.text = formatter.format(item.date)

        binding.root.setOnClickListener {
            clickCallback?.invoke(item)
        }
    }
}