package com.letsenvision.assignment.presentation.common

import androidx.databinding.DataBindingComponent
import androidx.fragment.app.Fragment
import com.letsenvision.assignment.di.Injectable

open class BaseFragment : Fragment(), Injectable {
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
}