package com.letsenvision.assignment.presentation.ui.main.capture

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.letsenvision.assignment.R
import com.letsenvision.assignment.databinding.OcrLoadingStateBinding

class OcrBusyDialog(context: Context) : Dialog(context) {

    private lateinit var binding: OcrLoadingStateBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.ocr_loading_state, null, false)
        setContentView(binding.root)
        binding.message = context.getString(R.string.ocr_in_progress)

        setCancelable(false)
        setCanceledOnTouchOutside(false)
    }
}