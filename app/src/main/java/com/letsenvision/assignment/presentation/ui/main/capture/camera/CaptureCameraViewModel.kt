package com.letsenvision.assignment.presentation.ui.main.capture.camera

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.letsenvision.assignment.common.enums.LoadingStatus
import com.letsenvision.assignment.common.helpers.Resource
import com.letsenvision.assignment.domain.model.DocumentModel
import com.letsenvision.assignment.domain.use_case.ReadDocumentUseCase
import com.letsenvision.assignment.presentation.common.SingleLiveEvent
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject

class CaptureCameraViewModel @Inject constructor(
    private val readDocumentUseCase: ReadDocumentUseCase
) : ViewModel() {

    private val _readDocumentResult: SingleLiveEvent<Resource<DocumentModel>> = SingleLiveEvent()

    var readDocumentResult: LiveData<Resource<DocumentModel>> = _readDocumentResult

    fun readDocument(file: File) = viewModelScope.launch {
        readDocumentUseCase.invoke(file).collect { values ->
            _readDocumentResult.value = values
        }
    }

    fun emitManualLoadingStatus(status: LoadingStatus) {
        _readDocumentResult.postValue(Resource(status = status, data = null, message = null))
    }
}