package com.letsenvision.assignment.presentation.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.letsenvision.assignment.R
import com.letsenvision.assignment.presentation.ui.main.capture.CaptureFragment
import com.letsenvision.assignment.presentation.ui.main.library.LibraryFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_capture_title,
    R.string.tab_library_title
)

class MainPagerAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        if (position == 0) {
            return CaptureFragment.newInstance();
        }
        return LibraryFragment.newInstance();
    }

    override fun getPageTitle(position: Int): CharSequence {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return TAB_TITLES.size
    }
}