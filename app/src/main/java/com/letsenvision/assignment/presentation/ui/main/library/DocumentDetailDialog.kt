package com.letsenvision.assignment.presentation.ui.main.library

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.letsenvision.assignment.R
import com.letsenvision.assignment.data.remote.dto.Paragraph
import com.letsenvision.assignment.databinding.DocumentDetailDialogBinding
import com.letsenvision.assignment.domain.model.DocumentModel
import java.text.SimpleDateFormat

class DocumentDetailDialog(context: Context, private val documentModel: DocumentModel) : Dialog(context) {

    private lateinit var binding: DocumentDetailDialogBinding

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.document_detail_dialog, null, false)
        setContentView(binding.root)
        setCanceledOnTouchOutside(false)
        binding.text = prepareText(documentModel.paragraphs)
        val formatter = SimpleDateFormat("dd/MM/yy HH:mm")
        binding.txtDate.text = formatter.format(documentModel.date)
        binding.btnClose.setOnClickListener {
            dismiss()
        }
    }

    private fun prepareText(pList: List<Paragraph>): String {
        var text = ""
        pList.forEach { x ->
            text += x.paragraph + " "
        }
        return text
    }
}