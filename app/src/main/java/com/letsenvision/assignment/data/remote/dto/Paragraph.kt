package com.letsenvision.assignment.data.remote.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Paragraph(
    val language: String,
    val paragraph: String
) : Parcelable