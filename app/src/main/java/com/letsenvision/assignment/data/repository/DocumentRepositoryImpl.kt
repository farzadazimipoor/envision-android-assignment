package com.letsenvision.assignment.data.repository

import androidx.annotation.WorkerThread
import com.letsenvision.assignment.common.extensions.createMultipartBody
import com.letsenvision.assignment.data.db.DocumentDao
import com.letsenvision.assignment.data.db.entity.DocumentEntity
import com.letsenvision.assignment.data.db.helper.DateTypeConverter
import com.letsenvision.assignment.data.remote.EnvisionApi
import com.letsenvision.assignment.data.remote.dto.toDocumentModel
import com.letsenvision.assignment.domain.model.DocumentModel
import com.letsenvision.assignment.domain.repository.DocumentRepository
import kotlinx.coroutines.flow.Flow
import java.io.File
import javax.inject.Inject

class DocumentRepositoryImpl @Inject constructor(
    private val api: EnvisionApi,
    private val dao: DocumentDao
) : DocumentRepository {
    override suspend fun readDocument(file: File): DocumentModel {
        val photo = file.createMultipartBody("photo", "image/*")
        return api.readDocument(photo).toDocumentModel()
    }

    @WorkerThread
    override suspend fun saveDocumentToLibrary(document: DocumentModel) {
        val entity = DocumentEntity(
            id = document.id,
            paragraphs = document.paragraphs,
            date = DateTypeConverter().fromDate(document.date)
        )
        dao.insert(entity)
    }

    override fun getDocumentsList(): Flow<List<DocumentEntity>> {
        return dao.getAll()
    }
}