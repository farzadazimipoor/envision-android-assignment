package com.letsenvision.assignment.data.db.helper

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.letsenvision.assignment.data.remote.dto.Paragraph

class ParagraphsTypeConverter {
    @TypeConverter
    fun toParagraphsList(jsonString: String): List<Paragraph> = Gson().fromJson(jsonString, Array<Paragraph>::class.java).toList()

    @TypeConverter
    fun toJsonString(list: List<Paragraph>): String = Gson().toJson(list.toTypedArray())
}