package com.letsenvision.assignment.data.db.entity

import android.annotation.SuppressLint
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.letsenvision.assignment.data.db.helper.DateTypeConverter
import com.letsenvision.assignment.data.db.helper.ParagraphsTypeConverter
import com.letsenvision.assignment.data.remote.dto.Paragraph
import com.letsenvision.assignment.domain.model.DocumentModel
import java.text.SimpleDateFormat

@Entity(tableName = "documents")
data class DocumentEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    @TypeConverters(ParagraphsTypeConverter::class)
    val paragraphs: List<Paragraph>,
    @TypeConverters(DateTypeConverter::class)
    var date: Long
)

@SuppressLint("SimpleDateFormat")
fun DocumentEntity.toDocumentModel(): DocumentModel {
    val addedDate = DateTypeConverter().toDate(date)
    return DocumentModel(
        id = id,
        paragraphs = paragraphs,
        date = addedDate
    )
}
