package com.letsenvision.assignment.data.remote.dto

import com.letsenvision.assignment.domain.model.DocumentModel
import java.util.*

data class ReadDocumentResultDto(
    val message: String,
    val response: ReadDocumentResponse?
)

data class ReadDocumentResponse(
    val paragraphs: List<Paragraph>
)

fun ReadDocumentResultDto.toDocumentModel(): DocumentModel {
    return DocumentModel(
        id = 0,
        paragraphs = response?.paragraphs ?: listOf(),
        date = Calendar.getInstance().time
    )
}