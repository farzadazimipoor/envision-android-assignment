package com.letsenvision.assignment.data.remote

import com.letsenvision.assignment.common.Constants
import com.letsenvision.assignment.data.remote.dto.ReadDocumentResultDto
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import java.util.concurrent.TimeUnit

interface EnvisionApi {

    @Multipart
    @POST("test/readDocument")
    suspend fun readDocument(@Part file: MultipartBody.Part): ReadDocumentResultDto

    companion object {
        private fun retrofit(): Retrofit {
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(HttpSupportInterceptor())
            httpClient.readTimeout(120, TimeUnit.SECONDS)
            httpClient.connectTimeout(120, TimeUnit.SECONDS)
            return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build()
        }

        fun create(): EnvisionApi {
            return retrofit().create(EnvisionApi::class.java)
        }
    }
}