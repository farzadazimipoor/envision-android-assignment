package com.letsenvision.assignment.data.db


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.letsenvision.assignment.data.db.entity.DocumentEntity
import com.letsenvision.assignment.data.db.helper.ParagraphsTypeConverter

@Database(
    entities = [DocumentEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(ParagraphsTypeConverter::class)
abstract class EnvisionDb : RoomDatabase() {
    companion object {
        fun create(context: Context, useInMemory: Boolean): EnvisionDb {
            val databaseBuilder = if (useInMemory) {
                Room.inMemoryDatabaseBuilder(context, EnvisionDb::class.java)
            } else {
                Room.databaseBuilder(context, EnvisionDb::class.java, "envision.db")
            }
            return databaseBuilder.fallbackToDestructiveMigration().build()
        }
    }

    abstract fun captures(): DocumentDao
}