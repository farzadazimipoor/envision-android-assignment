package com.letsenvision.assignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.letsenvision.assignment.common.helpers.Resource
import com.letsenvision.assignment.domain.model.DocumentModel
import com.letsenvision.assignment.domain.repository.DocumentRepository
import com.letsenvision.assignment.domain.use_case.ReadDocumentUseCase
import com.letsenvision.assignment.presentation.ui.main.capture.camera.CaptureCameraViewModel
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.io.File
import java.util.*
import org.mockito.Mockito.`when` as whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class CaptureViewModelUnitTest {
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var repository: DocumentRepository

    @Mock
    private lateinit var resultObserver: Observer<Resource<DocumentModel>>

    private lateinit var readDocumentUseCase: ReadDocumentUseCase

    private lateinit var viewModel: CaptureCameraViewModel

    @Before
    fun setUp() {
        readDocumentUseCase = ReadDocumentUseCase(repository)
        viewModel = CaptureCameraViewModel(readDocumentUseCase)
    }

    @Test
    fun `when fetching results ok then return a document model successfully`() {
        val fakeResult = DocumentModel(
            id = 0,
            paragraphs = listOf(),
            date = Calendar.getInstance().time
        )
        val file = mock(File::class.java)
        testCoroutineRule.runBlockingTest {
            viewModel.readDocumentResult.observeForever(resultObserver)
            whenever(repository.readDocument(file)).thenAnswer {
                fakeResult
            }
            viewModel.readDocument(file)
            assertNotNull(viewModel.readDocumentResult.value)
            assertEquals(Resource.success(data = fakeResult), viewModel.readDocumentResult.value)
            assertEquals(Resource.success(data = fakeResult).data, viewModel.readDocumentResult.value?.data)
        }
    }

    @Test
    fun `when calling for results then return loading`() {
        val file = mock(File::class.java)
        testCoroutineRule.runBlockingTest {
            viewModel.readDocumentResult.observeForever(resultObserver)
            viewModel.readDocument(file)
            verify(resultObserver).onChanged(Resource.loading(data = null))
        }
    }

    @Test
    fun `when fetching results fails then return an error`() {
        val exception = mock(Exception::class.java)
        val file = mock(File::class.java)
        testCoroutineRule.runBlockingTest {
            viewModel.readDocumentResult.observeForever(resultObserver)
            whenever(repository.readDocument(file)).thenAnswer {
                exception
            }
            viewModel.readDocument(file)
            assertNotNull(viewModel.readDocumentResult.value)
            assertNotNull(viewModel.readDocumentResult.value?.message)
            assertEquals(null, viewModel.readDocumentResult.value?.data)
        }
    }

    @After
    fun tearDown() {
        viewModel.readDocumentResult.removeObserver(resultObserver)
    }
}